import React, {useEffect, useState} from 'react'

import {withRouter} from 'react-router';


function Cart({}) {
    const [cartData, setCartData] = useState([]);
    const [cancel, setCancel ] = useState(1);

    useEffect(() => {
           getData()
    }, []);

   function getData() {
        let data = JSON.parse(localStorage.getItem('cart'))
        setCartData(data)
    }

    useEffect(() => {
            getData()
    }, [cancel]);


   function removeFromCart(e, index) {
      let cartItem = cartData
      cartItem.splice(index, 1)
       localStorage.setItem('cart', JSON.stringify(cartItem))
       setCancel(cancel+1)
      setCartData(cartItem)
   }
    return (
        <div className='mt-5 card p-4'>

            <div className='d-flex justify-content-between'>
                <div>
                    <h5 className='blue'>Cart</h5>
                </div>
            </div>

            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Availability</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    cartData.map((e, i) => {
                        return (
                            <tr>
                                <td>{e.date}</td>
                                <td>{e.time}</td>
                                <td>{e.avaiable}</td>
                                <td>
                                    <div className='btn btn-sm book-now' onClick={e => removeFromCart(e, i)}>Cancel</div>
                                </td>
                            </tr>
                        );
                    })
                }

                </tbody>
            </table>
        </div>
    )
}

export default withRouter(Cart)
