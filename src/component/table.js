import React, {useEffect, useState} from 'react'
import {withRouter} from 'react-router';
import * as moment from "moment";
import {toast} from "react-toastify";
import {useHistory} from "react-router";


const randomDate = () => {
    return moment(new Date(+(new Date()) - Math.floor(Math.random() * 10000000000)))
        .format('MM/DD/YYYY')
}

const getRandomZero = () => {
    return Math.floor(Math.random() * (Data.length - 0) + 0)
}

const Data = [
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
    {date: randomDate(), time: '01:00 PM PST- 2:00 PM PST', avaiable: Math.floor(Math.random() * (15 - 5) + 5)},
]



function List({}) {
    let history = useHistory();
    const [currentPageData, setCurrentPageData] = useState([Data]);
    const [bookingCoursePerWeek, setBookingCoursePerWeek] = useState([]);
    const [time, setTime] = useState();
    const [cart, setCart] = useState();

    useEffect(() => {
        setCurrentPageData(Data)
        let zero = getRandomZero()
        Data[zero].avaiable = 0
    }, []);


    useEffect(() => {
        countDown()
    }, []);

    function countDown() {
        var remainingTime = Math.floor(Math.random() * (60 - 30) + 30)
        var downloadTimer = setInterval(function () {
            if (remainingTime <= 0) {
                clearInterval(downloadTimer);
                countDown()
            }
            setTime(remainingTime)
            remainingTime -= 1;
        }, 1000);
    }

    function bookNow(data, index) {
        if (data.avaiable > 0) {
            var weeknumber = moment(data.date, "MM-DD-YYYY").week();
            if (bookingCoursePerWeek.length == 0) {
                let obj = {
                    week: weeknumber,
                    course: []
                }
                obj.course.push(data)
                Data[index].avaiable = data.avaiable - 1
                bookingCoursePerWeek.push(obj)
            } else {
                let flag = 0
                bookingCoursePerWeek.forEach(item => {
                    if (item.week == weeknumber) {
                        if (item.course.length < 3) {
                            item.course.push(data)
                            Data[index].avaiable = data.avaiable - 1
                            flag = 1
                        } else {
                            flag = 1
                            toast.error('You can book maximum 3 class per week')
                        }
                    }
                })
                if (flag == 0) {
                    let obj = {
                        week: weeknumber,
                        course: []
                    }
                    obj.course.push(data)
                    Data[index].avaiable = data.avaiable - 1
                    bookingCoursePerWeek.push(obj)
                }
            }
            setBookingCoursePerWeek(bookingCoursePerWeek)
        }
    }

    function gotoCart() {
        let list = []
        bookingCoursePerWeek.forEach(item => {
            item.course.forEach(course => {
                list.push(course)
            })
        })
        localStorage.setItem('cart', JSON.stringify(list))
        history.push('/cart')
    }


    return (
        <div className='mt-5 card p-4'>
            <div className='d-flex justify-content-between'>
                <div>
                    <strong>Time left: <span id='progressBar'>{time}</span> second</strong>
                    <h4 className='org'>Claim Your Free Trial Class</h4>
                </div>
                <div>
                    <img className='img-fluid cart' src='/shopping-cart.svg' role='button' onClick={e => gotoCart()}/>
                </div>
            </div>
            <br/>
            <br/>

            <div className='d-flex justify-content-between'>
                <div>
                    <h5 className='blue'>Class Schedule</h5>
                </div>
                <div>Free Seat Left: <span className='org'>7</span></div>
            </div>

            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Availability</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    currentPageData.map((e, i) => {
                        return (
                            <tr>
                                <td>{e.date}</td>
                                <td>{e.time}</td>
                                <td>{e.avaiable}</td>
                                <td>
                                    {
                                        e.avaiable !== 0 ? <div className='btn btn-sm book-now w-75'
                                                                onClick={event => bookNow(e, i)}>Book Now</div> :
                                            <div className='btn btn-sm full w-75'>Full</div>
                                    }
                                </td>
                            </tr>
                        );
                    })
                }

                </tbody>
            </table>
        </div>
    )
}

export default withRouter(List)
