import './App.css';
import Routes from './Routes';
import {
  BrowserRouter as Router,
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';

function App() {

    return (
        <Router>
            <div className="content-wrapper">
                <div className="content">
                    <Container fluid={true} className="app-bg">
                        <Routes />
                    </Container>
                </div>
            </div>
            <ToastContainer autoClose={5000}  />
        </Router>
  );
}

export default App;
