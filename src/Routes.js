import React from 'react';
import { Switch, Route } from 'react-router-dom';
import List from "./component/table";
import Cart from "./component/cart";

const Routes = ({}) => {
	return (
		<Switch>
			<Route exact path="/">
				<List/>
			</Route>
			<Route exact path="/cart">
				<Cart/>
			</Route>
		</Switch>
	);
};

export default Routes;
